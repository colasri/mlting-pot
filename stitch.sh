#!/usr/bin/env bash

FFMPEG="/opt/homebrew/bin/ffmpeg"
INPUT_VIDEO="./data/StateOfTheUnion_clip0_video_tagged_movements.mp4"
INPUT_AUDIO="./data/StateOfTheUnion_clip0_audio.mp3"
INPUT_SUB="./data/StateOfTheUnion_clip0_en_fr.srt"

DURATION=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 $INPUT_VIDEO)
WIDTH=$(ffprobe -v error -show_entries stream=width -of default=noprint_wrappers=1:nokey=1 $INPUT_VIDEO)
echo "Duration: $DURATION, width: $WIDTH"

# MP4, with sound, better resolution (still only 1/2 for reasonable file size)
OUTPUT_MP4="./data/StateOfTheUnion_clip0_final.mp4"
$FFMPEG -y -i $INPUT_VIDEO -i $INPUT_AUDIO -filter_complex \
    "color=c=0x00cc00:s=${WIDTH}x15[bar];\
    [0][bar]overlay=-w+(w/${DURATION})*t:H-h:shortest=1,subtitles=${INPUT_SUB},
    scale=w=iw/2:-1:flags=lanczos" \
    -c:a copy $OUTPUT_MP4

# Or GIF, small resolution, no sound, large file size...
# OUTPUT_GIF="./data/StateOfTheUnion_clip0_final.gif"
# ffmpeg -y -i $INPUT_VIDEO -filter_complex \
#     "color=c=0x00cc00:s=${WIDTH}x15[bar];\
#     [0][bar]overlay=-w+(w/${DURATION})*t:H-h:shortest=1,subtitles=${INPUT_SUB},
#     scale=-1:360:flags=lanczos,split[s0][s1],[s0]palettegen[p];[s1][p]paletteuse" \
#     -loop 0 $OUTPUT_GIF
