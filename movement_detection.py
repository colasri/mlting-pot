#!/usr/bin/env python

import cv2
import os
import tensorflow as tf
import wget

'''
# Dictionary that maps from joint names to keypoint indices.
KEYPOINT_DICT = {
    'nose': 0,
    'left_eye': 1,
    'right_eye': 2,
    'left_ear': 3,
    'right_ear': 4,
    'left_shoulder': 5,
    'right_shoulder': 6,
    'left_elbow': 7,
    'right_elbow': 8,
    'left_wrist': 9,
    'right_wrist': 10,
    'left_hip': 11,
    'right_hip': 12,
    'left_knee': 13,
    'right_knee': 14,
    'left_ankle': 15,
    'right_ankle': 16
}
'''

input_file = './data/StateOfTheUnion_clip0_video_tagged.mp4'

# open video file
video = cv2.VideoCapture(input_file)
if not video.isOpened():
    print("Could not open video file")
    exit()
fps = video.get(cv2.CAP_PROP_FPS)
print(f'FPS: {fps}')

video_writer = None
frame_counter = 0
frame_shape = None

# loop through frames
while video.isOpened():

    # Get a frame
    status, frame = video.read()
    if not status:
        print("End of video")
        break
    if frame_shape is None:
        frame_shape = frame.shape[:2]
        height = frame_shape[0]
        width = frame_shape[1]

    ############################################################################
    # Get keypoints
    ############################################################################
    image = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
    input_image = tf.expand_dims(image, axis=0)

    input_image = tf.image.resize_with_pad(input_image, 192, 192)

    model_path = "./data/movenet_lightning_fp16.tflite"
    # !wget -q -O model.tflite https://tfhub.dev/google/lite-model/movenet/singlepose/lightning/tflite/float16/4?lite-format=tflite
    wget.download('https://tfhub.dev/google/lite-model/movenet/singlepose/lightning/tflite/float16/4?lite-format=tflite', model_path)
    interpreter = tf.lite.Interpreter(model_path)
    interpreter.allocate_tensors()

    input_image = tf.cast(input_image, dtype=tf.uint8)
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()
    interpreter.set_tensor(input_details[0]['index'], input_image.numpy())
    interpreter.invoke()
    keypoints = interpreter.get_tensor(output_details[0]['index'])

    ############################################################################
    # Generate the output frame
    ############################################################################
    KEYPOINT_EDGES = [(0, 1), (0, 2), (1, 3), (2, 4), (5, 7),
        (7, 9), (6, 8), (8, 10), (5, 6), (5, 11), (6, 12), (11, 12), (11, 13),
        (13, 15), (12, 14), (14, 16)]

    output_frame = frame

    # Dealing with factors for non-square images
    if width > height:
        scale = width
        delta_width = 0
        delta_height = -(width-height)/2
    else:
        scale = height
        delta_width = (width-height)/2
        delta_height = 0

    # Confidence threshold to draw keypoints and lines in color, else grey
    conf_thrs = 0.3

    for keypoint in keypoints[0][0]:
        x = int(keypoint[1] * scale + delta_width)
        y = int(keypoint[0] * scale + delta_height)
        confidence = keypoint[2]

        color = (0, 0, 255) # Red
        if confidence < conf_thrs:
            color = (128, 128, 128) # Grey
        cv2.circle(output_frame, (x, y), 4, color, -1)

    for edge in KEYPOINT_EDGES:
        confidence = min(keypoints[0][0][edge[0]][2], keypoints[0][0][edge[1]][2])
        color = (0, 255, 0) # Green
        if confidence < conf_thrs:
            color = (128, 128, 128) # Grey

        x1 = int(keypoints[0][0][edge[0]][1] * scale + delta_width)
        y1 = int(keypoints[0][0][edge[0]][0] * scale + delta_height)

        x2 = int(keypoints[0][0][edge[1]][1] * scale + delta_width)
        y2 = int(keypoints[0][0][edge[1]][0] * scale + delta_height)

        cv2.line(output_frame, (x1, y1), (x2, y2), color, 2)

    if video_writer is None:
        # Define the codec and create VideoWriter object
        fourcc = cv2.VideoWriter_fourcc(*'avc1')
        output_file = f'{os.path.splitext(input_file)[0]}_movements.mp4'
        video_writer = cv2.VideoWriter(output_file, fourcc, fps, (width, height))

    video_writer.write(output_frame)
    print(frame_counter)
    frame_counter += 1

video_writer.release()
print(f'Wrote file {output_file}')
cv2.destroyAllWindows()
