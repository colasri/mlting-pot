#!/usr/bin/env python

import os
import sys
import requests
from time import sleep
from pprint import pprint
import pickle

# API key should not be committed.
# Not committing it breaks CI tools though. I tried using a secret vault (HashiCorp Vault) but the
# free tier of gitlab does not allow using it.
# If anyone reading this has a solution, please shoot me an email!
api_key = os.environ['ASSEMBLYAI_API_KEY'] # Should not be committed.

input_file = './data/StateOfTheUnion_clip0_audio.mp3'
upload_endpoint = 'https://api.assemblyai.com/v2/upload'
transcript_endpoint = 'https://api.assemblyai.com/v2/transcript'

def read_audio_file(file):
    """Helper method that reads in audio files."""
    with open(file, 'rb') as f:
        while True:
            data = f.read(5242880)
            if not data:
                break
            yield data

headers = {
    'authorization': api_key,
    'content-type': 'application/json'
}

# Upload audio file
upload_response = requests.post(
    upload_endpoint,
    headers=headers,
    data=read_audio_file(input_file),
)
pprint(upload_response.json())
upload_url = upload_response.json()['upload_url']

# Transcript request
transcript_response = requests.post(
    transcript_endpoint,
    headers=headers,
    json={'audio_url': upload_url, "word_boost": ["families"], "boost_param": "high"},
)
pprint(transcript_response.json())

# Polling response
polling_endpoint = f"{transcript_endpoint}/{transcript_response.json()['id']}"
polling_response = requests.get(polling_endpoint, headers=headers)
while polling_response.json()['status'] != 'completed':
    sleep(5)
    polling_response = requests.get(polling_endpoint, headers=headers)
    print(f"Status: {polling_response.json()['status']}")

    if polling_response.json()['status'] == 'error':
        sys.exit('Audio file failed to process.')

# Saving result
output_file_extensionless = os.path.splitext(input_file)[0]
if False:
    # Some other output options, not used here

    # Saving result, word wise
    with open(output_file_extensionless+'.txt', 'w') as f:
        f.write(polling_response.json()['text'])
    with open(output_file_extensionless+'.pkl', 'wb') as f:
        pickle.dump(polling_response.json(), f)

    # Also directly subtitle file
    polling_response = requests.get(polling_endpoint+'/srt', headers=headers)
    with open(output_file_extensionless+'.srt', 'wb') as f:
        pickle.dump(polling_response.text, f)

# Getting whole sentences
polling_response = requests.get(polling_endpoint+'/sentences', headers=headers)
with open(output_file_extensionless+'_sentences.pkl', 'wb') as f:
    pickle.dump(polling_response.json(), f)
print(f'Transcript file saved under {output_file_extensionless}_sentences.pkl.')
