[![pipeline status](https://gitlab.com/colasri/mlting-pot/badges/main/pipeline.svg)](https://gitlab.com/colasri/mlting-pot/commits/main) [![Codacy Badge](https://app.codacy.com/project/badge/Grade/20bc9809d44448a9aeb6e70726520347)](https://www.codacy.com/gl/colasri/mlting-pot/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=colasri/mlting-pot&amp;utm_campaign=Badge_Grade)

# MLting pot

A bunch of ML steps put together.

## Description

Applying various ML techniques, mostly in python, working on a video (at the moment, an extract
of Joe Biden's first [State of the Union](https://www.youtube.com/watch?v=mVIXLQrC9rE)):

-   Natural Language Processing
    -   Audio transcript (AssemblyAI)
    -   English => French translation (Google T5 via transformers)
-   Computer Vision
    -   Object detection (YOLO via TensorFlow)
    -   Pose estimation (Google's [MoveNet](https://github.com/tensorflow/tfjs-models/tree/master/pose-detection/src/movenet) via TensorFlow)

And a bunch of things to stitch all that together, including OpenCV, PyTube, NumPy, MoviePy, FFMPEG,
Pickle, etc.

### Future ideas

Might probably not all come, but anyway:

-   NLP: summarize whole transcript
-   CV: Read sign language, lip reading
-   Update YOLO
-   Gender recognition
-   Emotions
-   Use PySparks
-   MoveNet:
    -   [Run inference with cropping algorithm](https://www.tensorflow.org/hub/tutorials/movenet#run_inference_with_cropping_algorithm) for better accuracy.
    -   Use more precise Thunder instead of faster Lightning.
-   Try BlazePose (33 landmarks) instead of MoveNet (17 landmarks),
-   Try body segmentation with [BodyPix](https://github.com/tensorflow/tfjs-models/tree/master/body-pix).
-   Use hard captions instead of `.srt` files? Easier for creating `.gif` example.
-   Add CI tools.
-   Optionally perform the audio transcript locally (probably with [facebook/wav2vec2-base-960h](https://huggingface.co/facebook/wav2vec2-base-960h) transformer?).

### Environment setup

```shell
brew install ffmpeg
conda create -n mlting -y python=3
conda activate mlting
conda install -y pytorch torchvision -c pytorch
pip install -r requirements.txt
```

The audio transcript uses [AssemblyAI](https://www.assemblyai.com), you need to register for an account (I used the free one) and export the API key to the ASSEMBLYAI_API_KEY environment variable (or put it directly on the python script, but don't commit it, it's secret).

```shell
$ cat ~/.zprofile
export ASSEMBLYAI_API_KEY="foobar1234567890"
```

## Workflow

The following scripts need to be run, in that order:

-   `dl_data.py`: Download video and audio from YouTube (PyTube), extract a short clip (MoviePy).
    -   input: None.
    -   output: video and audio files.
-   `audio_transcript.py`: Use [AssemblyAI](https://www.assemblyai.com) Speech-to-text API via HTTP
    post/get queries.
    -   input: Audio file.
    -   output: transcript, pickled list of sentences (and other info).
-   `subtitler_translator.py`: Create subtitles in different languages, using Google's
    [T5: Text-To-Text Transfer Transformer](https://github.com/google-research/text-to-text-transfer-transformer).
    -   input: pickled transcript.
    -   output: `.srt` subtitle file.
-   `object_detection.py`: Tag objects using YOLOv4.
    -   input: video.
    -   output: video.
-   `movement_detection.py`: Human pose estimation with MoveNet.
    -   input: video.
    -   output: video.
-   `stitch.sh`: Stitch audio back into new video with FFMPEG.
    -   input: video, audio, and subtitles.
    -   output: video (mp4 and gif, `libass` required in FFMPEG to hardcode subs).

## Result

Here is the result of this process, where subtitles, translation, item recognition and movement
detection were all performed using various ML techniques:

![State of the Union annotated clip](./result/StateOfTheUnion_clip0_final.mp4)

## Miscellaneous

The scripts were written on MacOS with Python 3.10. Updated to python 3.11, running on Apple Silicon M2.

-   The scripts should be OK for *nix, not on windows. Probably just need to edit
    the shebangs and paths, but I do not have a windows machine (or VM) to test so
    I'll stick to what works for me.
-   Scripts use f-strings, so Python should be at least 3.6.
