#!/usr/bin/env python

from pytube import YouTube
from moviepy.editor import VideoFileClip, AudioFileClip

output_path = './data'

video_list = {
#     'chandelier':
#     {
#         'url': 'https://www.youtube.com/watch?v=L62faWn-sa8',
#         'time ranges': [[77, 82], [150, 163]],
#     },
#     'dance':
#     {
#         'url': 'https://www.youtube.com/watch?v=iq3sC5BxsrE',
#         'time ranges': [[17*60, 17*60+40]],
#     },
    'StateOfTheUnion':
    {
        'url': 'https://www.youtube.com/watch?v=mVIXLQrC9rE',
        # 'time ranges': [[49*60+15, 49*60+51]],
        'time ranges': [[49*60+15, 49*60+16]],
    },
}

for title, params in video_list.items():
    # Download best quality audio and video
    print(title, params['url'], params['time ranges'])
    streams = YouTube(params['url']).streams
    video = streams.filter(type='video').order_by('resolution').desc().first()
    print(video)
    video.download(output_path=output_path, filename=f'{title}_video.mp4')
    audio = streams.filter(type='audio').order_by('abr').desc().first()
    print(audio)
    audio.download(output_path=output_path, filename=f'{title}_audio.mp4')

    # Cut short clips
    full_video = VideoFileClip(f'{output_path}/{title}_video.mp4')
    full_audio = AudioFileClip(f'{output_path}/{title}_audio.mp4')
    for i, time_range in enumerate(params['time ranges']):
        full_video.subclip(*time_range).write_videofile(f'{output_path}/{title}_clip{i}_video.mp4')
        full_audio.subclip(*time_range).write_audiofile(f'{output_path}/{title}_clip{i}_audio.mp3')
