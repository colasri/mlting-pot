#!/usr/bin/env python

import cvlib as cv
from cvlib.object_detection import draw_bbox
import cv2
import os
import pandas as pd


input_file = './data/StateOfTheUnion_clip0_video.mp4'

# open video file
video = cv2.VideoCapture(input_file)
if not video.isOpened():
    print("Could not open video file")
    exit()

fps = video.get(cv2.CAP_PROP_FPS)
print(f'FPS: {fps}')

video_writer = None
frame_shape = None
frame_counter = 0

# loop through frames
while video.isOpened():

    # Get a frame
    status, frame = video.read()
    if not status:
        print("End of video")
        break
    if frame_shape is None:
        frame_shape = frame.shape[:2]
        height = frame_shape[0]
        width = frame_shape[1]

    # apply object detection
    bbox, label, conf = cv.detect_common_objects(frame)
    # print(bbox, label, conf)

    # draw bounding box over detected objects
    output_frame = draw_bbox(frame, bbox, label, conf)

    # draw object count on the picture
    counts = pd.Series(label, dtype='str').value_counts()
    print(counts)
    person_number = counts.get('person', default=0)
    # sort alphabetically first in case of multiple objects with same value
    counts.sort_index(inplace=True)
    counts.sort_values(inplace=True, ascending=False)
    for i, (k, v) in enumerate(counts.items()):
        line = f'{k}: {v}'
        cv2.putText(
            output_frame, line,
            org=(70, 60+ 30*i),
            fontFace=cv2.FONT_HERSHEY_DUPLEX,
            fontScale=1,
            color=(0,255,0)
        )

    if video_writer is None:
        # Define the codec and create VideoWriter object
        fourcc = cv2.VideoWriter_fourcc(*'avc1')
        output_file = f'{os.path.splitext(input_file)[0]}_tagged.mp4'
        video_writer = cv2.VideoWriter(output_file, fourcc, fps, (width, height))

    video_writer.write(output_frame)
    print(frame_counter)
    frame_counter += 1
    # if frame_counter > 10:
    #     break

video_writer.release()
print(f'Wrote file {output_file}')
cv2.destroyAllWindows()
