#!/usr/bin/env python

import pickle
from transformers import pipeline

# Init translator
translator = pipeline('translation_en_to_fr', model='t5-base')

# Translator mini example
text = "Hello my friends, which language do you speak?"
translation = translator(text)
print(translation)

def format_time(sec):
    return f'{int(sec//3600):02}:{int(sec%3600//60):02}:{int(sec%60):02},{int(sec%1*1000):03}'


def write_subtitle_file(ifile, ofile, language='en'):
    '''
    Generate subtitle file with optional translation.

    Read sentences and times in pickled object from `audio_transcript.py`, and write in .srt
    format, with optional translation.
    Input language is assumed to be English.

    Keyword arguments:
    ifile -- input picke file name
    ofile -- output .srt file name
    language -- 'en', 'fr' or 'en_fr', output language, English, French or both
    '''
    with open(ofile, 'w') as g:
        with open(ifile, 'rb') as f:
            transcript = pickle.load(f)
            sentences = transcript['sentences']
            # print(type(sentences), sentences)
            for i, sentence in enumerate(sentences):
                output = f'{i+1}\n'
                output += f"{format_time(sentence['start']/1000)} --> " + \
                    f"{format_time(sentence['end']/1000)}\n"
                text = sentence['text']
                if language == 'en':
                    output += text + '\n\n'
                elif language == 'fr':
                    output += translator(text)[0]['translation_text'] + '\n\n'
                elif language == 'en_fr':
                    output += text + '\n'
                    output += '<font color=#fff000>' + translator(text)[0]['translation_text'] + \
                        '</font>' + '\n\n'
                else:
                    raise ValueError("Unknown language, must be one of ['en', 'fr', 'en fr'].")
                print(output)
                g.write(output)


if __name__ == '__main__':
    ifile = './data/StateOfTheUnion_clip0_audio_sentences.pkl'
    for language in ['en', 'fr', 'en_fr']:
        write_subtitle_file(
            ifile,
            f'./data/StateOfTheUnion_clip0_{language}.srt',
            language=language
        )
